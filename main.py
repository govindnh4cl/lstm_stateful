seed = 1337

# Setting randomness seed
import numpy as np
np.random.seed(seed)
import tensorflow as tf
tf.set_random_seed(seed)

from datetime import datetime
from lstm import my_factory
from logger import setup_logger


instance_id = datetime.now().strftime("%Y_%b_%d_%H-%M-%S")  # A time-stamp for log file, model disk-write etc
log = setup_logger(instance_id)

if __name__ == '__main__':
    exp_id = 0  # Supported values: 0, 1, 10, 11. See lstm.py for setting this
    predictor = my_factory(exp_id)  # Get appropriate precictor object based on exp_id
    predictor.train()
    # No separate test(). Testing is done by putting test set as validation set

