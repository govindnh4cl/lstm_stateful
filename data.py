import numpy as np


def get_data(exp_id, time_steps, num_train_samples, num_test_samples, batch_size):
    """
    Creates data according to exp_id
    :param exp_id: currently supported values: 0, 1, 10, 11
    :param time_steps:
    :param num_train_samples:
    :param num_test_samples:
    :param batch_size:
    :return: numpy arrays containing dataset
    """
    # Pre-allocating the numpy array for better readability
    X_train = np.zeros((num_train_samples, time_steps, 1), dtype=int)
    X_test = np.zeros((num_test_samples, time_steps, 1), dtype=int)
    y_train = np.zeros((num_train_samples), dtype=int)
    y_test = np.zeros((num_test_samples), dtype=int)

    # Setting internal time-steps to random numbers
    X_train[:, 1:] = np.random.randint(0, 2, (num_train_samples, time_steps-1, 1), dtype=int)
    X_test[:, 1:] = np.random.randint(0, 2, (num_test_samples, time_steps-1, 1), dtype=int)

    # Setting half of the first time-steps to 1
    one_indexes = np.random.choice(a=num_train_samples, size=int(num_train_samples / 2), replace=False)
    X_train[one_indexes, 0] = 1
    one_indexes = np.random.choice(a=num_test_samples, size=int(num_test_samples / 2), replace=False)
    X_test[one_indexes, 0] = 1

    # Creating labels
    if exp_id in (0, 1):
        # y = first entry of a sample
        y_train = X_train[:, 0, 0]
        y_test = X_test[:, 0, 0]
        '''
        Result:
            Stateless succeeds in first epochs itself.
            Stateful takes many epochs (didn't verify convergence)
        '''
    elif exp_id in (10, 11):
        # y = first entry of a collocated sample in previous batch
        y_train[batch_size:] = X_train[:num_train_samples - batch_size, 0, 0]
        y_test[batch_size:] = X_test[:num_test_samples - batch_size, 0, 0]
        '''
        Result:
            Stateless can't solve this problem
            Stateful also can't solve this problem. I have tried having batch_size of 1, but in vain.
        '''

    return X_train, y_train, X_test, y_test







