from abc import abstractmethod
import numpy as np
from keras.utils import print_summary
from keras.models import Sequential
from keras.layers import Dense, LSTM
from keras.optimizers import RMSprop, Adagrad

# User defined imports
from data import get_data
from logger import get_logger


log = get_logger()


def my_factory(exp_id):
    class_dict = {
        0: LSTM_Stateless,
        1: LSTM_Stateful,

        10: LSTM_Stateless,  # Have current batch depend on previous batch
        11: LSTM_Stateful,  # Have current batch depend on previous batch
    }

    return class_dict[exp_id](exp_id)


class LSTM_Base():
    """
    Base class for LSTM models
    """
    def __init__(self, exp_id):
        self.exp_id = exp_id
        self.m = Sequential()
        ## ---- User configurable parameters start -----
        self.num_epochs = 5
        self.batch_size = 4
        self.time_steps = 5
        self.num_units = 3
        self.loss_function = 'mse'
        self.metrics = ['mse', 'acc']

        self.num_train_samples = 1024
        self.num_test_samples = 256
        ## ---- User configurable parameters end -----

        # Get data into numpy arrays
        self.X_train, self.y_train, self.X_test, self.y_test \
            = get_data(self.exp_id, self.time_steps, self.num_train_samples,
                       self.num_test_samples, self.batch_size)

    def get_optimizer(self):
        """
        Optimizer could be specific to base class
        :return: optimizer object
        """
        opt = RMSprop(lr=0.02)
        return opt

    def train(self):
        self.m.compile(loss=self.loss_function, optimizer=self.get_optimizer(), metrics=self.metrics)

        for i in range(self.num_epochs):
            hist = self.m.fit(self.X_train, self.y_train,
                              batch_size=self.batch_size,
                              validation_data = (self.X_test, self.y_test),
                              epochs=1,
                              shuffle=False,
                              verbose=0)

            log.info('Epoch: {:d} (Loss, Acc) Train: ({:.2f}, {:.2f})  Test: ({:.2f}, {:.2f})'.
                     format(i, hist.history['loss'][0], hist.history['acc'][0],
                            hist.history['val_loss'][0], hist.history['val_acc'][0]))
            self.m.reset_states()  # Manually reset all LSTM states after each epoch

    def print_config(self):
        """
        Prints current configuration using logger. Also saves to log file
        :return: None
        """
        log.info('Summary:')
        log.info('=====================================')
        log.info('exp_id: {:d}'.format(self.exp_id))
        log.info('batch_size: {:d}'.format(self.batch_size))
        log.info('time_steps: {:d}'.format(self.time_steps))
        log.info('num_units: {:d}'.format(self.num_units))
        log.info('num_train_samples: {:d} num_batches: {:.1f}'.format(self.num_train_samples, self.num_train_samples/self.batch_size))
        log.info('num_test_samples: {:d} num_batches: {:.1f}'.format(self.num_test_samples, self.num_test_samples/self.batch_size))
        print_summary(self.m, print_fn=log.info)
        log.info('=====================================')

class LSTM_Stateless(LSTM_Base):
    """
    Derived class for 'stateless' LSTM
    """
    def __init__(self, exp_id):
        LSTM_Base.__init__(self, exp_id)
        log.info('Using Stateless LSTM')
        self.m.add(LSTM(self.num_units, batch_input_shape=(self.batch_size, self.time_steps, 1)))
        self.m.add(Dense(1, activation='sigmoid'))
        self.print_config()


class LSTM_Stateful(LSTM_Base):
    """
    Derived class for 'stateful' LSTM
    """
    def __init__(self, exp_id):
        LSTM_Base.__init__(self, exp_id)
        log.info('Using Stateful LSTM')
        self.m.add(LSTM(self.num_units, batch_input_shape=(self.batch_size, self.time_steps, 1), stateful=True))
        self.m.add(Dense(1, activation='sigmoid'))
        self.print_config()

